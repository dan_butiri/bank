package com.butirialexandrudan.bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SavingAccount extends Account implements Serializable{
	private static final long serialVersionUID = -6470090944414208496L;
	private double soldSavingAccount;
	private double soldDisponibilRetragere;
	private double dobandaSavingAccount;
	private List<String> tranzactii;

	public SavingAccount() {
		soldSavingAccount = 0;
		soldDisponibilRetragere = 0;
		dobandaSavingAccount = 0;
		tranzactii = new ArrayList<String>();
	}

	public double getSoldSavingAccount() {
		return soldSavingAccount;
	}

	public void setSoldSavingAccount(double soldSavingAccount) {
		this.soldSavingAccount = soldSavingAccount;
	}

	public double getDobandaSavingAccount() {
		return dobandaSavingAccount;
	}

	public void setDobandaSavingAccount(double dobandaSavingAccount) {
		this.dobandaSavingAccount = dobandaSavingAccount;
	}

	public void addTranzactie(String s) {
		tranzactii.add(s);
	}

	public List<String> getTranzactii() {
		return tranzactii;
	}

	public double getSoldDisponibilRetragere() {
		return soldDisponibilRetragere;
	}

	public void setSoldDisponibilRetragere(double soldDisponibilRetragere) {
		this.soldDisponibilRetragere = soldDisponibilRetragere;
	}
}
