package com.butirialexandrudan.bank;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class AdminControl {

	private AdminPanel adminPanel;
	private NewAccountPanel newAccountPanel;
	private Bank bank;
	private DefaultListModel<String> dlm;

	public AdminControl(AdminPanel newAdminPanel, Bank newBank) {
		adminPanel = newAdminPanel;
		newAccountPanel = new NewAccountPanel();
		bank = newBank;
		dlm = adminPanel.getDlm();

		adminPanel.getMyTable().addNewTable(
				new ArrayList<Object>(bank.cauta("")));

		adminPanel.getBtnAddAccount().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				addBtn();
			}
		});

		adminPanel.getBtnRemoveAccount().addActionListener(
				new ActionListener() {

					public void actionPerformed(ActionEvent ae) {
						removeBtn();
					}
				});

		adminPanel.getBtnCalculeazaDobanda().addActionListener(
				new ActionListener() {

					public void actionPerformed(ActionEvent ae) {
						calculeazaDobanda();

						int row = adminPanel.getTable().getSelectedRow();
						if (row >= 0) {
							int idAccount = Integer
									.parseInt(adminPanel.getMyTable()
											.getValueAt(row, 3).toString());

							if (bank.getAccount(idAccount) != null) {
								completeazaDetaliiList(bank
										.getAccount(idAccount));
							}
						}
					}
				});

		adminPanel.getBtnEditAccount().addActionListener(new ActionListener() {

			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent ae) {
				EditAccountPanel editAccountPanel;
				Account account;
				boolean test;
				String text = "";
				int row = adminPanel.getTable().getSelectedRow();
				if (row >= 0) {
					int idAccount = Integer.parseInt(adminPanel.getMyTable()
							.getValueAt(row, 3).toString());

					if (bank.getAccount(idAccount) != null) {
						account = bank.getAccount(idAccount);
						editAccountPanel = new EditAccountPanel(account);

						JOptionPane.showMessageDialog(null, editAccountPanel,
								"Modifica Cont", JOptionPane.QUESTION_MESSAGE);

						text = editAccountPanel.getTfName().getText();
						test = text.compareTo(" Nume" + '\0') != 0;
						account.getPerson().setName(
								test ? text : account.getPerson().getName());

						text = editAccountPanel.getTfId().getText();
						test = text.compareTo(" CNP" + '\0') != 0;
						account.getPerson().setId(
								test ? text : account.getPerson().getId());

						text = editAccountPanel.getTfAddress().getText();
						test = text.compareTo(" Adresa" + '\0') != 0;
						account.getPerson().setAddress(
								test ? text : account.getPerson().getAddress());

						text = editAccountPanel.getPfPassword().getText();
						test = text.compareTo(" Parola" + '\0') != 0;
						account.setPassword(test ? text : account.getPassword());

						text = editAccountPanel.getTfSumaBani().getText();
						try {
							double suma = Double.parseDouble(text);

							if (suma >= 0) {
								if (editAccountPanel.getRbtnDepunere()
										.isSelected()) {
									if (SavingAccount.class.isInstance(account)) {
										((SavingAccount) account).setSoldSavingAccount(((SavingAccount) account)
												.getDobandaSavingAccount()
												+ suma);
									}
									if (SpendingAccount.class
											.isInstance(account)) {
										((SpendingAccount) account)
												.setSold(((SpendingAccount) account)
														.getSold() + suma);
									}
								}
								if (editAccountPanel.getRbtnRetragere()
										.isSelected()) {
									if (SavingAccount.class.isInstance(account)) {
										((SavingAccount) account).setSoldSavingAccount(((SavingAccount) account)
												.getDobandaSavingAccount()
												- suma);
									}
									if (SpendingAccount.class
											.isInstance(account)) {
										((SpendingAccount) account)
												.setSold(((SpendingAccount) account)
														.getSold() - suma);
									}
								}
							} else {
								JOptionPane
										.showMessageDialog(
												null,
												"Suma de bani trebuie sa fie pozitiva...",
												"Avertizment",
												JOptionPane.WARNING_MESSAGE);
							}
						} catch (Exception e) {
						}

						if (SavingAccount.class.isInstance(account)) {
							text = editAccountPanel.getTfDobanda().getText();
							try {
								double dobanda = Double.parseDouble(text);

								if (dobanda >= 0) {
									((SavingAccount) account)
											.setDobandaSavingAccount(dobanda);
								} else {
									JOptionPane
											.showMessageDialog(
													null,
													"Dobanda trebuie sa fie pozitiva...",
													"Avertizment",
													JOptionPane.WARNING_MESSAGE);
								}
							} catch (Exception e) {
							}
						}

						account.setIsLocked(editAccountPanel.getIsLocked()
								.isSelected());

						adminPanel.getMyTable().addNewTable(
								new ArrayList<Object>(bank.cauta("")));
						completeazaDetaliiList(account);
					}
				}
			}
		});

		adminPanel.getTfCauta().addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent ke) {
				String ch = adminPanel.getTfCauta().getText();
				if ((int) ke.getKeyChar() != KeyEvent.VK_BACK_SPACE) {
					ch = adminPanel.getTfCauta().getText() + ke.getKeyChar();
				}
				if (ch.compareTo(" Cauta" + '\0') == 0) {
					ch = "";
				}
				adminPanel.getMyTable().addNewTable(
						new ArrayList<Object>(bank.cauta(ch)));
			}
		});

		adminPanel.getTable().addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent me) {

				int idAccount = Integer.parseInt(adminPanel.getMyTable()
						.getValueAt(adminPanel.getTable().getSelectedRow(), 3)
						.toString());
				completeazaDetaliiList(bank.getAccount(idAccount));

			}
		});

		adminPanel.getTable().addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent arg) {
				int idAccount = Integer.parseInt(adminPanel.getMyTable()
						.getValueAt(adminPanel.getTable().getSelectedRow(), 3)
						.toString());
				completeazaDetaliiList(bank.getAccount(idAccount));
			}

			public void mousePressed(MouseEvent arg) {
				int idAccount = Integer.parseInt(adminPanel.getMyTable()
						.getValueAt(adminPanel.getTable().getSelectedRow(), 3)
						.toString());
				completeazaDetaliiList(bank.getAccount(idAccount));
			}
		});

		adminPanel.getTable().addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				if (ke.getKeyCode() == KeyEvent.VK_UP
						|| ke.getKeyCode() == KeyEvent.VK_DOWN) {
					int idAccount = Integer.parseInt(adminPanel
							.getMyTable()
							.getValueAt(adminPanel.getTable().getSelectedRow(),
									3).toString());
					completeazaDetaliiList(bank.getAccount(idAccount));
				}
			}

			public void keyPressed(KeyEvent ke) {
				if (ke.getKeyCode() == KeyEvent.VK_UP
						|| ke.getKeyCode() == KeyEvent.VK_DOWN) {
					int idAccount = Integer.parseInt(adminPanel
							.getMyTable()
							.getValueAt(adminPanel.getTable().getSelectedRow(),
									3).toString());
					completeazaDetaliiList(bank.getAccount(idAccount));
				}
			}
		});
	}

	@SuppressWarnings("deprecation")
	private void addBtn() {
		Person person = new Person();
		String error = "";

		JOptionPane.showMessageDialog(null, newAccountPanel, "New Account",
				JOptionPane.QUESTION_MESSAGE);

		if (newAccountPanel.getTfName().getText().compareTo(" Nume" + '\0') != 0) {
			person.setName(newAccountPanel.getTfName().getText());
		} else {
			error += "Nume\n";
			newAccountPanel.getTfName().setText(" Nume" + '\0');
		}
		if (newAccountPanel.getTfId().getText().compareTo(" Id" + '\0') != 0) {
			person.setId(newAccountPanel.getTfId().getText());
		} else {
			error += "Id\n";
			newAccountPanel.getTfId().setText(" Id" + '\0');
		}
		if (newAccountPanel.getTfAddress().getText()
				.compareTo(" Adresa" + '\0') != 0) {
			person.setAddress(newAccountPanel.getTfAddress().getText());
		} else {
			error += "Adresa\n";
			newAccountPanel.getTfAddress().setText(" Adresa" + '\0');
		}
		if (newAccountPanel.getPfPassword().getText()
				.compareTo(" Password" + '\0') == 0) {
			error += "Parola\n";
			newAccountPanel.getPfPassword().setText(" Password" + '\0');
			newAccountPanel.getPfPassword().setEchoChar((char) 0);
		}
		if (error.length() != 0) {
			error += "\nContul nu a fost creat!!!";
			JOptionPane.showMessageDialog(null, "Campuri invalide:\n" + error,
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else {
			if (newAccountPanel.getIsSavingAccount().isSelected()) {
				SavingAccount sa = new SavingAccount();

				sa.setPassword(newAccountPanel.getPfPassword().getText());
				// Generare id.
				sa.setId(bank.maxIdAccount() + 1);
				person.setIdAccount(bank.maxIdAccount() + 1);
				sa.setPerson(person);
				bank.addAccount(sa);
			}
			if (newAccountPanel.getIsSpendingAccount().isSelected()) {
				SpendingAccount sa = new SpendingAccount();
				Person p = new Person();

				p.setAddress(person.getAddress());
				p.setId(person.getId());
				p.setIdAccount(person.getIdAccount());
				p.setName(person.getName());

				sa.setPassword(newAccountPanel.getPfPassword().getText());
				// Generare id.
				sa.setId(bank.maxIdAccount() + 1);
				p.setIdAccount(bank.maxIdAccount() + 1);
				sa.setPerson(p);
				bank.addAccount(sa);
			}

			adminPanel.getMyTable().addNewTable(
					new ArrayList<Object>(bank.getPersonArrayList()));

			newAccountPanel.getTfName().setText(" Nume" + '\0');
			newAccountPanel.getTfName().setForeground(Color.LIGHT_GRAY);
			newAccountPanel.getTfId().setText(" Id" + '\0');
			newAccountPanel.getTfId().setForeground(Color.LIGHT_GRAY);
			newAccountPanel.getTfAddress().setText(" Adresa" + '\0');
			newAccountPanel.getTfAddress().setForeground(Color.LIGHT_GRAY);
			newAccountPanel.getPfPassword().setText(" Password" + '\0');
			newAccountPanel.getPfPassword().setEchoChar((char) 0);
			newAccountPanel.getPfPassword().setForeground(Color.LIGHT_GRAY);
			newAccountPanel.getIsSavingAccount().setSelected(false);
			newAccountPanel.getIsSpendingAccount().setSelected(false);
		}
	}

	private void removeBtn() {
		int row = adminPanel.getTable().getSelectedRow();
		if (row >= 0) {
			int idAccount = Integer.parseInt(adminPanel.getMyTable()
					.getValueAt(row, 3).toString());
			bank.removeAccount(bank.getAccount(idAccount));
			adminPanel.getMyTable().addNewTable(
					new ArrayList<Object>(bank.getPersonArrayList()));
		}
	}

	private void calculeazaDobanda() {
		for (Account account : bank.getAccounts()) {
			if (SavingAccount.class.isInstance(account)) {
				String tranzactie = "";
				Calendar data = Calendar.getInstance();
				double suma = ((SavingAccount) account)
						.getSoldDisponibilRetragere();

				suma += ((((SavingAccount) account).getSoldSavingAccount() * ((SavingAccount) account)
						.getDobandaSavingAccount()) / 100);

				((SavingAccount) account).setSoldDisponibilRetragere(suma);
				((SavingAccount) account).setDataMade(data);

				tranzactie += (data.get(Calendar.YEAR) + "/");
				tranzactie += ((data.get(Calendar.MONTH) + 1) + "/");
				tranzactie += (data.get(Calendar.DAY_OF_MONTH));
				tranzactie += (" (" + data.get(Calendar.HOUR) + ":");
				tranzactie += (data.get(Calendar.MINUTE) + ") ");
				tranzactie += ("Recalculare, suma: " + suma);

				((SavingAccount) account).addTranzactie(tranzactie);
			}
		}
	}

	private void completeazaDetaliiList(Account account) {
		dlm.removeAllElements();
		dlm.addElement("------------------Despre client----------------------");
		dlm.addElement("Nume: " + account.getPerson().getName());
		dlm.addElement("Adresa: " + account.getPerson().getAddress());
		dlm.addElement("CNP: " + account.getPerson().getId());
		dlm.addElement("ID Account: " + account.getPerson().getIdAccount());
		dlm.addElement("------------------Despre cont------------------------");
		String data = "" + account.getDataMade().get(Calendar.DAY_OF_MONTH)
				+ "/" + (account.getDataMade().get(Calendar.MONTH) + 1) + "/"
				+ account.getDataMade().get(Calendar.YEAR);
		dlm.addElement("Creat in data: " + data);
		dlm.addElement("Cont blocat: " + account.getIsLocked());
		if (SavingAccount.class.isInstance(account)) {
			dlm.addElement("Tipul contului: Saving Account");
			dlm.addElement("Sold blocat: "
					+ ((SavingAccount) account).getSoldSavingAccount());
			dlm.addElement("Sold disponibil: "
					+ ((SavingAccount) account).getSoldDisponibilRetragere());
			dlm.addElement("Data calculat soldul: " + data);
			dlm.addElement("Dobanda: "
					+ ((SavingAccount) account).getDobandaSavingAccount());
			dlm.addElement("------------------Tranzactii---------------------------");
			for (String tranzactie : ((SavingAccount) account).getTranzactii()) {
				dlm.addElement("-> " + tranzactie);
			}
		}
		if (SpendingAccount.class.isInstance(account)) {
			dlm.addElement("Tipul contului: Spending Account");
			dlm.addElement("Bani in cont: "
					+ ((SpendingAccount) account).getSold());
			dlm.addElement("------------------Tranzactii---------------------------");
			for (String tranzactie : ((SpendingAccount) account)
					.getTranzactii()) {
				dlm.addElement("-> " + tranzactie);
			}
		}
	}
}
