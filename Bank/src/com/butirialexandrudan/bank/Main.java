package com.butirialexandrudan.bank;

public class Main {

	public static void main(String[] args) {

		LogInFrame logInFrame = new LogInFrame();
		Bank bank = new Bank();
		logInFrame.setVisible(true);
		
		new LogInControl(logInFrame, bank);
	}

}
