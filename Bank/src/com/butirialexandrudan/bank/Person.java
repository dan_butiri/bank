package com.butirialexandrudan.bank;

import java.io.Serializable;

public class Person implements Serializable{
	private static final long serialVersionUID = -6470090944414208496L;
	@AddThis
	private String name;
	@AddThis
	private String id;
	@AddThis
	private String address;
	@AddThis
	private int idAccount;

	public Person() {
		name = "";
		id = "";
		address = "";
	}

	public Person(String newName, String newId, String newAddress) {
		name = newName;
		id = newId;
		address = newAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(int idAccount) {
		this.idAccount = idAccount;
	}
}
