package com.butirialexandrudan.bank;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Annotation pentru classa MyTable. Pentru campurile care sa apara in tabel.
 * Target FIELD. Retention RUNTIME.
 * 
 */
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface AddThis {

}
