package com.butirialexandrudan.bank;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class EditAccountPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private JPanel pnlPersoana;
	private JPanel pnlCont;
	private JTextField tfName;
	private JTextField tfId;
	private JTextField tfAddress;
	private JTextField tfSumaBani;
	private JTextField tfDobanda;
	private JPasswordField pfPassword;
	private JRadioButton rbtnDepunere;
	private JRadioButton rbtnRetragere;
	private JCheckBox isLocked;
	private ButtonGroup group;
	private GridBagConstraints gbc;
	private Account account;

	/**
	 * Create the panel.
	 */
	public EditAccountPanel(Account newAccount) {
		account = newAccount;
		pnlPersoana = new JPanel();
		pnlCont = new JPanel();
		tfName = new JTextField(account.getPerson().getName());
		tfId = new JTextField(account.getPerson().getId());
		tfAddress = new JTextField(account.getPerson().getAddress());
		tfSumaBani = new JTextField("0.0");
		if (SavingAccount.class.isInstance(account)) {
			tfDobanda = new JTextField("" + ((SavingAccount)account).getDobandaSavingAccount());
		} else {
			tfDobanda = new JTextField("0.0");
			tfDobanda.setVisible(false);
		}
		pfPassword = new JPasswordField(account.getPassword());
		rbtnDepunere = new JRadioButton("Depunere");
		rbtnRetragere = new JRadioButton("Retragere");
		isLocked = new JCheckBox("Blocat");
		group = new ButtonGroup();
		gbc = new GridBagConstraints();
		
		isLocked.setSelected(account.getIsLocked());
		rbtnDepunere.setSelected(true);
		group.add(rbtnDepunere);
		group.add(rbtnRetragere);
		this.add(pnlPersoana);
		this.add(pnlCont);
		
		pnlPersoana.setLayout(new GridBagLayout());
		pnlCont.setLayout(new GridBagLayout());
		
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;

		pnlPersoana.setBorder(BorderFactory.createTitledBorder(null, "Date personale",
				TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, null));
		
		gbc.ipadx = 150;
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 0;
		pnlPersoana.add(tfName, gbc);
//		addMessageName();

		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 1;
		pnlPersoana.add(tfId, gbc);
//		addMessageId();

		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 2;
		pnlPersoana.add(tfAddress, gbc);
//		addMessageAddress();
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 3;
		pnlPersoana.add(pfPassword, gbc);
//		addMessagePassword();
		
		pnlCont.setBorder(BorderFactory.createTitledBorder(null, "Date cont",
				TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, null));
		
		gbc.ipadx = 0;
		gbc.gridwidth = 2;
		gbc.gridx = 2;
		gbc.gridy = 0;
		pnlCont.add(tfSumaBani, gbc);

		gbc.gridwidth = 1;
		gbc.gridx = 2;
		gbc.gridy = 1;
		pnlCont.add(rbtnDepunere, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 3;
		gbc.gridy = 1;
		pnlCont.add(rbtnRetragere, gbc);
		
		gbc.gridwidth = 2;
		gbc.gridx = 2;
		gbc.gridy = 2;
		pnlCont.add(isLocked, gbc);
		
		gbc.gridwidth = 2;
		gbc.gridx = 2;
		gbc.gridy = 3;
		pnlCont.add(tfDobanda, gbc);
	}

	public JTextField getTfName() {
		return tfName;
	}

	public JTextField getTfId() {
		return tfId;
	}

	public JTextField getTfAddress() {
		return tfAddress;
	}

	public JTextField getTfSumaBani() {
		return tfSumaBani;
	}

	public JTextField getTfDobanda() {
		return tfDobanda;
	}

	public JPasswordField getPfPassword() {
		return pfPassword;
	}

	public JRadioButton getRbtnDepunere() {
		return rbtnDepunere;
	}

	public JRadioButton getRbtnRetragere() {
		return rbtnRetragere;
	}

	public JCheckBox getIsLocked() {
		return isLocked;
	}
}
