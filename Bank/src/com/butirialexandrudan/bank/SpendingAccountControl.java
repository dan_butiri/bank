package com.butirialexandrudan.bank;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JOptionPane;

public class SpendingAccountControl {
	SpendingAccount spendingAccount;
	SpendingAccountPanel spendingAccountPanel;
	Bank bank;

	public SpendingAccountControl(SpendingAccount newSpendingAccount,
			SpendingAccountPanel newSpendingAccountPanel, Bank newBank) {
		spendingAccount = newSpendingAccount;
		spendingAccountPanel = newSpendingAccountPanel;
		bank = newBank;

		spendingAccountPanel.getBtnAdaugaSold().addActionListener(
				new ActionListener() {

					public void actionPerformed(ActionEvent ae) {

						addSold();

					}
				});

		spendingAccountPanel.getBtnRetrageSold().addActionListener(
				new ActionListener() {

					public void actionPerformed(ActionEvent ae) {

						retrageSuma();

					}
				});

		spendingAccountPanel.getBtnTransfer().addActionListener(
				new ActionListener() {

					public void actionPerformed(ActionEvent ae) {

						transferSuma();

					}
				});
	}

	private void addSold() {
		double suma = 0;
		String tranzactie = "";
		Calendar data = Calendar.getInstance();

		try {
			suma = Double.parseDouble(spendingAccountPanel.getTfSumaBani()
					.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie numar",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		}

		if (suma < 0) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie pozitiva",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else {
			if (!spendingAccount.getIsLocked()) {
				spendingAccount.setSold(spendingAccount.getSold() + suma);

				tranzactie += (data.get(Calendar.YEAR) + "/");
				tranzactie += ((data.get(Calendar.MONTH) + 1) + "/");
				tranzactie += (data.get(Calendar.DAY_OF_MONTH) + "/");
				tranzactie += (" (" + data.get(Calendar.HOUR) + ":");
				tranzactie += (data.get(Calendar.MINUTE) + ") ");
				tranzactie += ("Depunere, suma: " + suma);
				spendingAccount.addTranzactie(tranzactie);
				spendingAccountPanel.getLblSold().setText(
						"" + spendingAccount.getSold());
			} else {
				JOptionPane.showMessageDialog(null,
						"Contul este blocat\nTranzactia nu a reusit...",
						"Avertizment", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private void retrageSuma() {
		double suma = 0;
		String tranzactie = "";
		Calendar data = Calendar.getInstance();

		try {
			suma = Double.parseDouble(spendingAccountPanel.getTfSumaBani()
					.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie numar",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		}

		if (suma < 0) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie pozitiva",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else if (suma > spendingAccount.getSold()) {
			JOptionPane.showMessageDialog(null, "Suma indisponibila",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else {
			if (!spendingAccount.getIsLocked()) {
				spendingAccount.setSold(spendingAccount.getSold() - suma);

				tranzactie += (data.get(Calendar.YEAR) + "/");
				tranzactie += ((data.get(Calendar.MONTH) + 1) + "/");
				tranzactie += (data.get(Calendar.DAY_OF_MONTH) + "/");
				tranzactie += (" (" + data.get(Calendar.HOUR) + ":");
				tranzactie += (data.get(Calendar.MINUTE) + ") ");
				tranzactie += ("Retragere, suma: " + suma);
				spendingAccount.addTranzactie(tranzactie);
				spendingAccountPanel.getLblSold().setText(
						"" + spendingAccount.getSold());
			} else {
				JOptionPane.showMessageDialog(null,
						"Contul este blocat\nTranzactia nu a reusit...",
						"Avertizment", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private void transferSuma() {
		double suma = 0;
		int idAccountTransfer = 0;
		Account accountTransfer = null;
		String tranzactie1 = "", tranzactie2 = "";
		Calendar data = Calendar.getInstance();

		try {
			suma = Double.parseDouble(spendingAccountPanel.getTfSumaBani()
					.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie numar",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		}

		try {
			idAccountTransfer = Integer.parseInt(spendingAccountPanel
					.getTfIdAccountTransfer().getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"ID-ul contului pe care se transfera trebuie sa fie numar",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		}

		if (suma < 0) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie pozitiva",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else if (suma > spendingAccount.getSold()) {
			JOptionPane.showMessageDialog(null, "Suma indisponibila",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else {
			accountTransfer = bank.getAccount(idAccountTransfer);
			if (accountTransfer == null) {
				JOptionPane.showMessageDialog(null, "Contul nu exista",
						"Avertizment", JOptionPane.WARNING_MESSAGE);
			} else {
				if (SavingAccount.class.isInstance(accountTransfer)) {
					JOptionPane
							.showMessageDialog(
									null,
									"Transferul nu se poate face pe un Saving Account...",
									"Avertizment", JOptionPane.WARNING_MESSAGE);
				} else {
					if (spendingAccount.getId() != idAccountTransfer) {
						if (!spendingAccount.getIsLocked()) {
							spendingAccount.setSold(spendingAccount.getSold()
									- suma);
							((SpendingAccount) accountTransfer)
									.setSold(((SpendingAccount) accountTransfer)
											.getSold() + suma);

							tranzactie1 += (data.get(Calendar.YEAR) + "/");
							tranzactie1 += ((data.get(Calendar.MONTH) + 1) + "/");
							tranzactie1 += (data.get(Calendar.DAY_OF_MONTH) + "/");
							tranzactie1 += (" (" + data.get(Calendar.HOUR) + ":");
							tranzactie1 += (data.get(Calendar.MINUTE) + ") ");
							tranzactie1 += ("Transfer in cont "
									+ idAccountTransfer + ", suma: " + suma);
							spendingAccount.addTranzactie(tranzactie1);

							tranzactie2 += (data.get(Calendar.YEAR) + "/");
							tranzactie2 += ((data.get(Calendar.MONTH) + 1) + "/");
							tranzactie2 += (data.get(Calendar.DAY_OF_MONTH) + "/");
							tranzactie2 += (" (" + data.get(Calendar.HOUR) + ":");
							tranzactie2 += (data.get(Calendar.MINUTE) + ") ");
							tranzactie2 += ("Transfer din cont "
									+ spendingAccount.getId() + ", suma: " + suma);
							((SpendingAccount) accountTransfer)
									.addTranzactie(tranzactie2);

							spendingAccountPanel.getLblSold().setText(
									"" + spendingAccount.getSold());
						} else {
							JOptionPane
									.showMessageDialog(
											null,
											"Contul este blocat\nTranzactia nu a reusit...",
											"Avertizment",
											JOptionPane.WARNING_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null,
								"Tranzactie inutila...\n\n"
										+ "Transfer din contul "
										+ idAccountTransfer + " in cont "
										+ idAccountTransfer, "Avertizment",
								JOptionPane.WARNING_MESSAGE);
					}
				}
			}
		}
	}
}
