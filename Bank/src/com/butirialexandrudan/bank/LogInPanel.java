package com.butirialexandrudan.bank;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LogInPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private JTextField tfIdAccount;
	private JPasswordField pfPassword;
	private JButton btnLogIn;
	private JButton btnAdmin;
	private GridBagConstraints gbc;

	/**
	 * Create the panel.
	 */
	public LogInPanel() {
		tfIdAccount = new JTextField(" ID Account" + '\0');
		pfPassword = new JPasswordField(" Password" + '\0');
		btnLogIn = new JButton("Log In");
		btnAdmin = new JButton("Admin!");
		gbc = new GridBagConstraints();

		this.setLayout(new GridBagLayout());
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5, 5, 5, 5);
		
		gbc.ipadx = 300;
		gbc.gridwidth = 4;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(tfIdAccount, gbc);
		addMessageCNP();
		
		gbc.ipadx = 300;
		gbc.gridwidth = 4;
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(pfPassword, gbc);
		addMessagePassword();
		
		gbc.ipadx = 75;
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(btnAdmin, gbc);
		
		gbc.ipadx = 75;
		gbc.gridwidth = 2;
		gbc.gridx = 2;
		gbc.gridy = 2;
		this.add(btnLogIn, gbc);
	}

	public JTextField getTfIdAccount() {
		return tfIdAccount;
	}

	public JPasswordField getPfPassword() {
		return pfPassword;
	}

	public JButton getBtnLogIn() {
		return btnLogIn;
	}

	public JButton getBtnAdimn() {
		return btnAdmin;
	}
	
	private void addMessageCNP(){
		tfIdAccount.setForeground(Color.LIGHT_GRAY);
		tfIdAccount.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfIdAccount.setCaretPosition(0);

			}
		});
		tfIdAccount.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfIdAccount.getText().compareTo(" ID Account" + '\0') == 0) {
					tfIdAccount.setText("");
				}
				tfIdAccount.setForeground(Color.black);
				if (tfIdAccount.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfIdAccount.setText(" ID Account" + '\0');
					tfIdAccount.setCaretPosition(0);
					tfIdAccount.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfIdAccount.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfIdAccount.getText().compareTo(" ID Account" + '\0') == 0) {
					tfIdAccount.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfIdAccount.getText().compareTo(" ID Account" + '\0') == 0) {
					tfIdAccount.setCaretPosition(0);
				}
			}
		});
		tfIdAccount.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfIdAccount.getText().compareTo(" ID Account" + '\0') == 0) {
					tfIdAccount.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfIdAccount.getText().compareTo(" ID Account" + '\0') == 0) {
					tfIdAccount.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfIdAccount.getText().compareTo(" ID Account" + '\0') == 0) {
					tfIdAccount.setCaretPosition(0);
				}
			}
		});
	}
	
	private void addMessagePassword(){
		pfPassword.setForeground(Color.LIGHT_GRAY);
		pfPassword.setEchoChar((char)0);
		pfPassword.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				pfPassword.setCaretPosition(0);

			}
		});
		pfPassword.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("deprecation")
			public void keyTyped(KeyEvent ke) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setText("");
					pfPassword.setEchoChar('*');
				}
				pfPassword.setForeground(Color.black);
				if (pfPassword.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					pfPassword.setText(" Password" + '\0');
					pfPassword.setEchoChar((char)0);
					pfPassword.setCaretPosition(0);
					pfPassword.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		pfPassword.addMouseMotionListener(new MouseMotionListener() {

			@SuppressWarnings("deprecation")
			public void mouseMoved(MouseEvent me) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}

			@SuppressWarnings("deprecation")
			public void mouseDragged(MouseEvent me) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}
		});
		pfPassword.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("deprecation")
			public void mouseClicked(MouseEvent ae) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}

			@SuppressWarnings("deprecation")
			public void mousePressed(MouseEvent ae) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}

			@SuppressWarnings("deprecation")
			public void mouseReleased(MouseEvent ae) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}
		});
	}
}
