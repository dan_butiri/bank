package com.butirialexandrudan.bank;

public interface BankProc {

	public void addAccount(Account newAccount);

	public void removeAccount(Account newAccount);

	public void readAccount(String fileName);

	public void writeAccount(String fileName);

}
