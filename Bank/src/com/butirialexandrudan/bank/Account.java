package com.butirialexandrudan.bank;

import java.io.Serializable;
import java.util.Calendar;

public class Account  implements Serializable{
	private static final long serialVersionUID = -6470090944414208496L;
	private int id;
	private Person person;
	private String password;
	private Calendar dataMade;
	private Boolean isLocked;

	public Account() {
		person = null;
		password = "";
		dataMade = Calendar.getInstance();
//		System.out.println(Calendar.getInstance().get(Calendar.MONTH));
		isLocked = false;
	}

	public Account(Person newPerson, String newPassword) {
		person = newPerson;
		password = newPassword;
		dataMade = Calendar.getInstance();
		isLocked = false;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Calendar getDataMade() {
		return dataMade;
	}

	public void setDataMade(Calendar dataMade) {
		this.dataMade = dataMade;
	}

	public Boolean getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
