package com.butirialexandrudan.bank;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;

public class LogInControl {

	private LogInPanel logInPanel;
	private LogInFrame logInFrame;
	private AdminFrame adminFrame;
	private UserFrame userFrame;
	private Bank bank;

	public LogInControl(LogInFrame newLogInFrame, Bank newBank) {
		logInFrame = newLogInFrame;
		logInPanel = logInFrame.getLogInPanel();
		adminFrame = new AdminFrame();
		userFrame = null;
		bank = newBank;

		new AdminControl(adminFrame.getAdminPanel(), bank);

		logInPanel.getBtnAdimn().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				adminFrame.setVisible(true);
				//adminFrame.setResizable(false);
				adminFrame.setLocation(300, 100);
				adminFrame.pack();
				logInFrame.setVisible(false);

				adminFrame.addWindowListener(new WindowAdapter() {
					public void windowClosed(WindowEvent we) {
						logInFrame.setVisible(true);
					}
				});

			}
		});

		logInPanel.getBtnLogIn().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int idAccount;
				try {
					idAccount = Integer.parseInt(logInPanel.getTfIdAccount()
							.getText());
				} catch (Exception e) {
					idAccount = 0;
				}
				@SuppressWarnings("deprecation")
				String password = logInPanel.getPfPassword().getText();
				Account account = bank.getAccount(idAccount, password);

				if (account != null) {
					userFrame = new UserFrame(bank.getAccount(idAccount,
							password));
					if (SpendingAccount.class.isInstance(account)) {
						new SpendingAccountControl((SpendingAccount) account, userFrame
								.getUserPanel().getSpendingAccountPanel(), bank);
					}
					if (SavingAccount.class.isInstance(account)) {
						new SavingAccountControl((SavingAccount) account, userFrame
								.getUserPanel().getSavingAccountPanel());
					}
					userFrame.setVisible(true);
					logInFrame.setVisible(false);
					userFrame.setLocation(logInFrame.getLocation());

					userFrame.addWindowListener(new WindowAdapter() {
						public void windowClosed(WindowEvent we) {
							logInFrame.setVisible(true);
							logInFrame.setLocation(userFrame.getLocation());
						}
					});
					
					userFrame.addWindowListener(new WindowAdapter() {
						public void windowClosing(WindowEvent we) {
							bank.writeAccount("accounts.ser");
						}
					});
				} else {
					JOptionPane.showMessageDialog(null, "Date invalide!",
							"Avertizare", JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		
		logInFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				bank.writeAccount("accounts.ser");
			}
		});
	}
}
