package com.butirialexandrudan.bank;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Bank implements BankProc, Serializable {

	private static final long serialVersionUID = -6470090944414208496L;
	private Set<Account> accounts;

	public Bank() {
		readAccount("accounts.ser");
	}

	/**
	 * @pre newAccount != null
	 * @post size + 1 == accounts.size()
	 */
	public void addAccount(Account newAccount) {
		assert (newAccount != null);
		assert wellFormed();
		int size = accounts.size();
		accounts.add(newAccount);
		assert wellFormed();
		assert (size + 1 == accounts.size());
	}

	/**
	 * @pre newAccount != null
	 * @post size - 1 == accounts.size()
	 */
	public void removeAccount(Account newAccount) {
		assert (newAccount != null);
		assert wellFormed();
		int size = accounts.size();
		accounts.remove(newAccount);
		assert wellFormed();
		assert (size - 1 == accounts.size());
	}

	// deserialize to Object from given file
	/**
	 * @pre fileName != null
	 * @post accounts != null
	 */
	@SuppressWarnings({ "resource", "unchecked" })
	public void readAccount(String fileName) {
		assert (fileName != null);
		assert wellFormed();
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
					fileName));

			accounts = new HashSet<Account>(
					(Collection<? extends Account>) ois.readObject());
		} catch (Exception e) {
			accounts = new HashSet<Account>();
		}
		assert wellFormed();
		assert (accounts != null);
	}

	// serialize the given object and save it to file
	/**
	 * @pre fileName != null
	 * @post
	 */
	@SuppressWarnings("resource")
	public void writeAccount(String fileName) {
		assert (fileName != null);
		assert wellFormed();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(fileName));

			oos.writeObject(accounts);
		} catch (Exception e) {

		}
		assert wellFormed();
	}

	protected boolean wellFormed() {
		if (accounts != null)
			return true;
		else
			return false;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public Account getAccount(int idAccount, String password) {

		for (Account account : accounts) {
			if (account.getId() == idAccount
					&& account.getPassword().compareTo(password) == 0) {
				return account;
			}
		}

		return null;
	}

	public Account getAccount(int id) {
		for (Account account : accounts) {
			if (account.getId() == id)
				return account;
		}
		return null;
	}

	public ArrayList<Person> getPersonArrayList() {
		ArrayList<Person> persons = new ArrayList<Person>();

		for (Account account : accounts) {
			persons.add(account.getPerson());
		}

		return persons;
	}

	public ArrayList<Person> cauta(String ch) {
		ArrayList<Person> persons = new ArrayList<Person>();

		for (Account account : accounts) {
			if (account.getPerson().getName().toLowerCase()
					.contains(ch.toLowerCase())) {
				persons.add(account.getPerson());
			}
		}

		return persons;
	}

	public int maxIdAccount() {
		int max = 0;
		if (accounts.isEmpty())
			return 0;
		for (Account account : accounts) {
			max = max < account.getId() ? account.getId() : max;
		}
		return max;
	}

	@Override
	public String toString() {
		return "[ " + this.getClass().getName() + " Object { " + accounts + "}";
	}
}
