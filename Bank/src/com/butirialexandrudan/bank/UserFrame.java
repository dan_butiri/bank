package com.butirialexandrudan.bank;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class UserFrame extends JFrame {

	private static final long serialVersionUID = -6470090944414208496L;
	private UserPanel userPanel;
	private JFrame frame;
	private Account account;

	/**
	 * Create the frame.
	 */
	public UserFrame(Account newAccount) {
		account = newAccount;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		userPanel = new UserPanel(account);
		setContentPane(userPanel);
		setResizable(false);
		setTitle("User");
		pack();
		frame = this;

		userPanel.getTabbedPane().addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent ce) {
				if (userPanel
						.getTabbedPane()
						.getTitleAt(
								userPanel.getTabbedPane().getSelectedIndex())
						.compareTo("Log Out") == 0) {
					if (JOptionPane.showConfirmDialog(null, "Esti sigur? :)",
							"Avertizment", JOptionPane.YES_NO_OPTION) == 0) {
						setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						frame.dispose();
					} else {
						userPanel.getTabbedPane().setSelectedComponent(
								userPanel.getTabbedPane().getTabComponentAt(0));
					}
				}
				userPanel.getAboutAccountPanel()
						.completeazaDetaliiList(account);
				if (SpendingAccount.class.isInstance(account)) {
					userPanel
							.getSpendingAccountPanel()
							.getLblSold()
							.setText("" + ((SpendingAccount) account).getSold());
				}
				if (SavingAccount.class.isInstance(account)) {
					userPanel
							.getSavingAccountPanel()
							.getLblSoldDisponibila()
							.setText(
									""
											+ ((SavingAccount) account)
													.getSoldDisponibilRetragere());
				}
			}
		});
	}

	public UserPanel getUserPanel() {
		return userPanel;
	}
}
