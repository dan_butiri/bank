package com.butirialexandrudan.bank;

import javax.swing.JFrame;

public class AdminFrame extends JFrame {

	private static final long serialVersionUID = -6470090944414208496L;
	private AdminPanel adminPanel;

	/**
	 * Create the frame.
	 */
	public AdminFrame() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		adminPanel = new AdminPanel();
		setContentPane(adminPanel);
		setTitle("Admin");
		pack();
	}

	public AdminPanel getAdminPanel() {
		return adminPanel;
	}
}
