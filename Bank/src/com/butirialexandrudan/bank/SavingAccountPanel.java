package com.butirialexandrudan.bank;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SavingAccountPanel extends JPanel {
	
	private static final long serialVersionUID = -6470090944414208496L;
	private JLabel lblTextSoldDisponibila;
	private JLabel lblSoldDisponibila;
	private JTextField tfRetragereSuma;
	private JButton btnRetrageSuma;
	private GridBagConstraints gbc;

	/**
	 * Create the panel.
	 */
	public SavingAccountPanel() {
		lblTextSoldDisponibila = new JLabel("Sold: ");
		lblSoldDisponibila = new JLabel("0");
		tfRetragereSuma = new JTextField(" Suma bani" + '\0');
		btnRetrageSuma = new JButton("Retrage");
		gbc = new GridBagConstraints();

		this.setLayout(new GridBagLayout());
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5, 5, 5, 5);
		
		gbc.ipadx = 50;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(lblTextSoldDisponibila, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(lblSoldDisponibila, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 2;
		gbc.gridy = 0;
		add(btnRetrageSuma, gbc);
		
		gbc.ipadx = 200;
		gbc.gridwidth = 4;
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(tfRetragereSuma, gbc);
		addMessageSuma();
	}
	
	public JLabel getLblSoldDisponibila() {
		return lblSoldDisponibila;
	}

	public JTextField getTfRetragereSuma() {
		return tfRetragereSuma;
	}

	public JButton getBtnRetrageSuma() {
		return btnRetrageSuma;
	}

	private void addMessageSuma(){
		tfRetragereSuma.setForeground(Color.LIGHT_GRAY);
		tfRetragereSuma.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfRetragereSuma.setCaretPosition(0);

			}
		});
		tfRetragereSuma.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfRetragereSuma.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfRetragereSuma.setText("");
				}
				tfRetragereSuma.setForeground(Color.black);
				if (tfRetragereSuma.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfRetragereSuma.setText(" Suma bani" + '\0');
					tfRetragereSuma.setCaretPosition(0);
					tfRetragereSuma.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfRetragereSuma.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfRetragereSuma.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfRetragereSuma.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfRetragereSuma.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfRetragereSuma.setCaretPosition(0);
				}
			}
		});
		tfRetragereSuma.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfRetragereSuma.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfRetragereSuma.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfRetragereSuma.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfRetragereSuma.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfRetragereSuma.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfRetragereSuma.setCaretPosition(0);
				}
			}
		});
	}
}
