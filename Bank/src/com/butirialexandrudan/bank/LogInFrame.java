package com.butirialexandrudan.bank;


import javax.swing.JFrame;

public class LogInFrame extends JFrame {

	private static final long serialVersionUID = -6470090944414208496L;
	private LogInPanel logInPanel;

	/**
	 * Create the frame.
	 */
	public LogInFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Log In");
		logInPanel = new LogInPanel();
		setContentPane(logInPanel);
		setResizable(false);
		pack();
		setLocation(500, 200);
	}

	public LogInPanel getLogInPanel() {
		return logInPanel;
	}
}
