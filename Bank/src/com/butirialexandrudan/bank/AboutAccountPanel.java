package com.butirialexandrudan.bank;

import java.awt.GridLayout;
import java.util.Calendar;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class AboutAccountPanel extends JPanel {
	private static final long serialVersionUID = -6470090944414208496L;
	private DefaultListModel<String> dlm;
	private JList<String> personList;
	private JScrollPane scrollPane;

	/**
	 * Create the panel.
	 */
	public AboutAccountPanel(Account newAccount) {
		dlm = new DefaultListModel<String>();
		personList = new JList<String>(dlm);
		scrollPane = new JScrollPane(personList);
		setLayout(new GridLayout(1, 1));

		add(scrollPane);
		completeazaDetaliiList(newAccount);
	}

	public void completeazaDetaliiList(Account account) {
		dlm.removeAllElements();
		dlm.addElement("------------------Despre client----------------------");
		dlm.addElement("Nume: " + account.getPerson().getName());
		dlm.addElement("Adresa: " + account.getPerson().getAddress());
		dlm.addElement("CNP: " + account.getPerson().getId());
		dlm.addElement("ID Account: " + account.getPerson().getIdAccount());
		dlm.addElement("------------------Despre cont------------------------");
		String data = "" + account.getDataMade().get(Calendar.DAY_OF_MONTH)
				+ "/" + (account.getDataMade().get(Calendar.MONTH) + 1) + "/"
				+ account.getDataMade().get(Calendar.YEAR);
		dlm.addElement("Creat in data: " + data);
		dlm.addElement("Cont blocat: " + account.getIsLocked());
		if (SavingAccount.class.isInstance(account)) {
			dlm.addElement("Tipul contului: Saving Account");
			dlm.addElement("Sold blocat: "
					+ ((SavingAccount) account).getSoldSavingAccount());
			dlm.addElement("Sold disponibil: "
					+ ((SavingAccount) account).getSoldDisponibilRetragere());
			dlm.addElement("Data calculari soldul: " + data);
			dlm.addElement("Dobanda: "
					+ ((SavingAccount) account).getDobandaSavingAccount());
			dlm.addElement("------------------Tranzactii---------------------------");
			for (String tranzactie : ((SavingAccount) account).getTranzactii()) {
				dlm.addElement("-> " + tranzactie);
			}
		}
		if (SpendingAccount.class.isInstance(account)) {
			dlm.addElement("Tipul contului: Spending Account");
			dlm.addElement("Sold: " + ((SpendingAccount) account).getSold());
			dlm.addElement("------------------Tranzactii---------------------------");
			for (String tranzactie : ((SpendingAccount) account)
					.getTranzactii()) {
				dlm.addElement("-> " + tranzactie);
			}
		}
	}
}
