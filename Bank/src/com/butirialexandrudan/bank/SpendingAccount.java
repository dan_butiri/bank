package com.butirialexandrudan.bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SpendingAccount extends Account implements Serializable {
	private static final long serialVersionUID = -6470090944414208496L;
	private double sold;
	private List<String> tranzactii;

	public SpendingAccount() {
		sold = 0;
		tranzactii = new ArrayList<String>();
	}

	public double getSold() {
		return sold;
	}

	public void setSold(double suma) {
		this.sold = suma;
	}

	public void addTranzactie(String s) {
		tranzactii.add(s);
	}

	public List<String> getTranzactii() {
		return tranzactii;
	}
}
