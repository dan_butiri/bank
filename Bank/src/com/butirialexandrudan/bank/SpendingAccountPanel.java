package com.butirialexandrudan.bank;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SpendingAccountPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private JLabel lblTextSold;
	private JLabel lblSold;
	private JTextField tfSumaBani;
	private JTextField tfIdAccountTransfer;
	private JButton btnAdaugaSold;
	private JButton btnRetrageSold;
	private JButton btnTransfer;
	private GridBagConstraints gbc;

	/**
	 * Create the panel.
	 */
	public SpendingAccountPanel() {
		lblTextSold = new JLabel("Sold: ");
		lblSold = new JLabel("0");
		tfSumaBani = new JTextField(" Suma bani" + '\0');
		tfIdAccountTransfer = new JTextField(" ID cont transfer" + '\0');
		btnAdaugaSold = new JButton("Adauga");
		btnRetrageSold = new JButton("Retrage");
		btnTransfer = new JButton("Transfera");
		gbc = new GridBagConstraints();

		this.setLayout(new GridBagLayout());
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5, 5, 5, 5);
		
		gbc.gridwidth = 3;
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(lblTextSold, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 3;
		gbc.gridy = 0;
		add(lblSold, gbc);
		
		gbc.ipadx = 200;
		gbc.gridwidth = 4;
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(tfSumaBani, gbc);
		addMessageSuma();
		
		gbc.gridwidth = 4;
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(tfIdAccountTransfer, gbc);
		addMessageTransfer();
		
		gbc.ipadx = 30;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 3;
		add(btnAdaugaSold, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 3;
		add(btnRetrageSold, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 3;
		gbc.gridy = 3;
		add(btnTransfer, gbc);
	}

	public JTextField getTfSumaBani() {
		return tfSumaBani;
	}

	public JTextField getTfIdAccountTransfer() {
		return tfIdAccountTransfer;
	}

	public JButton getBtnAdaugaSold() {
		return btnAdaugaSold;
	}

	public JButton getBtnRetrageSold() {
		return btnRetrageSold;
	}

	public JButton getBtnTransfer() {
		return btnTransfer;
	}
	
	public JLabel getLblSold() {
		return lblSold;
	}

	private void addMessageSuma(){
		tfSumaBani.setForeground(Color.LIGHT_GRAY);
		tfSumaBani.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfSumaBani.setCaretPosition(0);

			}
		});
		tfSumaBani.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfSumaBani.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfSumaBani.setText("");
				}
				tfSumaBani.setForeground(Color.black);
				if (tfSumaBani.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfSumaBani.setText(" Suma bani" + '\0');
					tfSumaBani.setCaretPosition(0);
					tfSumaBani.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfSumaBani.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfSumaBani.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfSumaBani.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfSumaBani.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfSumaBani.setCaretPosition(0);
				}
			}
		});
		tfSumaBani.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfSumaBani.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfSumaBani.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfSumaBani.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfSumaBani.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfSumaBani.getText().compareTo(" Suma bani" + '\0') == 0) {
					tfSumaBani.setCaretPosition(0);
				}
			}
		});
	}
	
	private void addMessageTransfer(){
		tfIdAccountTransfer.setForeground(Color.LIGHT_GRAY);
		tfIdAccountTransfer.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfIdAccountTransfer.setCaretPosition(0);

			}
		});
		tfIdAccountTransfer.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfIdAccountTransfer.getText().compareTo(" ID cont transfer" + '\0') == 0) {
					tfIdAccountTransfer.setText("");
				}
				tfIdAccountTransfer.setForeground(Color.black);
				if (tfIdAccountTransfer.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfIdAccountTransfer.setText(" ID cont transfer" + '\0');
					tfIdAccountTransfer.setCaretPosition(0);
					tfIdAccountTransfer.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfIdAccountTransfer.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfIdAccountTransfer.getText().compareTo(" ID cont transfer" + '\0') == 0) {
					tfIdAccountTransfer.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfIdAccountTransfer.getText().compareTo(" ID cont transfer" + '\0') == 0) {
					tfIdAccountTransfer.setCaretPosition(0);
				}
			}
		});
		tfIdAccountTransfer.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfIdAccountTransfer.getText().compareTo(" ID cont transfer" + '\0') == 0) {
					tfIdAccountTransfer.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfIdAccountTransfer.getText().compareTo(" ID cont transfer" + '\0') == 0) {
					tfIdAccountTransfer.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfIdAccountTransfer.getText().compareTo(" ID cont transfer" + '\0') == 0) {
					tfIdAccountTransfer.setCaretPosition(0);
				}
			}
		});
	}
}
