package com.butirialexandrudan.bank;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class UserPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private JTabbedPane tabbedPane;
	private Account account;
	private AboutAccountPanel aboutAccountPanel;
	private SavingAccountPanel savingAccountPanel;
	private SpendingAccountPanel spendingAccountPanel;

	/**
	 * Create the panel.
	 */
	public UserPanel(Account newAccount) {
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		account = newAccount;
		aboutAccountPanel = new AboutAccountPanel(account);
		savingAccountPanel = new SavingAccountPanel();
		spendingAccountPanel = new SpendingAccountPanel();

		this.setLayout(new GridLayout(1, 1));

		tabbedPane.addTab(newAccount.getPerson().getName(), aboutAccountPanel);

		if (SavingAccount.class.isInstance(account)) {
			tabbedPane.addTab("Saving Account", savingAccountPanel);
		}
		if (SpendingAccount.class.isInstance(account)) {
			tabbedPane.addTab("Spending Account", spendingAccountPanel);
		}

		tabbedPane.addTab("Log Out", null);

		add(tabbedPane);
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public SavingAccountPanel getSavingAccountPanel() {
		return savingAccountPanel;
	}

	public SpendingAccountPanel getSpendingAccountPanel() {
		return spendingAccountPanel;
	}

	public AboutAccountPanel getAboutAccountPanel() {
		return aboutAccountPanel;
	}
}
