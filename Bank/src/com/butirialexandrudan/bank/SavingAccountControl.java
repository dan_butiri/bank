package com.butirialexandrudan.bank;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JOptionPane;

public class SavingAccountControl {
	SavingAccount savingAccount;
	SavingAccountPanel savingAccountPanel;

	public SavingAccountControl(SavingAccount newSavingAccount,
			SavingAccountPanel newSavingAccountPanel) {
		savingAccount = newSavingAccount;
		savingAccountPanel = newSavingAccountPanel;

		savingAccountPanel.getBtnRetrageSuma().addActionListener(
				new ActionListener() {

					public void actionPerformed(ActionEvent ae) {

						retrageSoldDisponibil();

					}
				});
	}

	private void retrageSoldDisponibil() {
		double suma = 0;
		String tranzactie = "";
		Calendar data = Calendar.getInstance();

		try {
			suma = Double.parseDouble(savingAccountPanel.getTfRetragereSuma()
					.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie numar",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		}

		if (suma < 0) {
			JOptionPane.showMessageDialog(null, "Suma trebuie sa fie pozitiva",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else if (suma > savingAccount.getSoldDisponibilRetragere()) {
			JOptionPane.showMessageDialog(null, "Suma indisponibila",
					"Avertizment", JOptionPane.WARNING_MESSAGE);
		} else {
			if (!savingAccount.getIsLocked()) {
				savingAccount.setSoldDisponibilRetragere(savingAccount
						.getSoldDisponibilRetragere() - suma);

				tranzactie += (data.get(Calendar.YEAR) + "/");
				tranzactie += ((data.get(Calendar.MONTH) + 1) + "/");
				tranzactie += (data.get(Calendar.DAY_OF_MONTH) + "/");
				tranzactie += (" (" + data.get(Calendar.HOUR) + ":");
				tranzactie += (data.get(Calendar.MINUTE) + ") ");
				tranzactie += ("Retragere, suma: " + suma);
				savingAccount.addTranzactie(tranzactie);
				savingAccountPanel.getLblSoldDisponibila().setText(
						"" + savingAccount.getSoldDisponibilRetragere());
			} else {
				JOptionPane.showMessageDialog(null,
						"Contul este blocat\nTranzactia nu a reusit...",
						"Avertizment", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}
