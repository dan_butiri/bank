package com.butirialexandrudan.bank;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class NewAccountPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private JTextField tfName;
	private JTextField tfId;
	private JTextField tfAddress;
	private JPasswordField pfPassword;
	private JCheckBox isSavingAccount;
	private JCheckBox isSpendingAccount;
	private GridBagConstraints gbc;

	/**
	 * Create the panel.
	 */
	public NewAccountPanel() {
		tfName = new JTextField(" Nume" + '\0');
		tfId = new JTextField(" Id" + '\0');
		tfAddress = new JTextField(" Adresa" + '\0');
		pfPassword = new JPasswordField(" Password" + '\0');
		isSavingAccount = new JCheckBox("Saving Account");
		isSpendingAccount = new JCheckBox("Spending Account");
		gbc = new GridBagConstraints();

		this.setLayout(new GridBagLayout());
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(tfName, gbc);
		addMessageName();

		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(tfId, gbc);
		addMessageId();

		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(tfAddress, gbc);
		addMessageAddress();
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 3;
		add(pfPassword, gbc);
		addMessagePassword();
		
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 4;
		add(isSavingAccount, gbc);

		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 4;
		add(isSpendingAccount, gbc);
	}

	public JTextField getTfName() {
		return tfName;
	}

	public JTextField getTfId() {
		return tfId;
	}

	public JTextField getTfAddress() {
		return tfAddress;
	}

	public JCheckBox getIsSavingAccount() {
		return isSavingAccount;
	}

	public JCheckBox getIsSpendingAccount() {
		return isSpendingAccount;
	}

	public JPasswordField getPfPassword() {
		return pfPassword;
	}

	private void addMessageName() {
		tfName.setForeground(Color.LIGHT_GRAY);
		tfName.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfName.setCaretPosition(0);

			}
		});
		tfName.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfName.getText().compareTo(" Nume" + '\0') == 0) {
					tfName.setText("");
				}
				tfName.setForeground(Color.black);
				if (tfName.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfName.setText(" Nume" + '\0');
					tfName.setCaretPosition(0);
					tfName.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfName.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfName.getText().compareTo(" Nume" + '\0') == 0) {
					tfName.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfName.getText().compareTo(" Nume" + '\0') == 0) {
					tfName.setCaretPosition(0);
				}
			}
		});
		tfName.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfName.getText().compareTo(" Nume" + '\0') == 0) {
					tfName.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfName.getText().compareTo(" Nume" + '\0') == 0) {
					tfName.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfName.getText().compareTo(" Nume" + '\0') == 0) {
					tfName.setCaretPosition(0);
				}
			}
		});
	}

	private void addMessageId() {
		tfId.setForeground(Color.LIGHT_GRAY);
		tfId.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfId.setCaretPosition(0);

			}
		});
		tfId.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfId.getText().compareTo(" Id" + '\0') == 0) {
					tfId.setText("");
				}
				tfId.setForeground(Color.black);
				if (tfId.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfId.setText(" Id" + '\0');
					tfId.setCaretPosition(0);
					tfId.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfId.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfId.getText().compareTo(" Id" + '\0') == 0) {
					tfId.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfId.getText().compareTo(" Id" + '\0') == 0) {
					tfId.setCaretPosition(0);
				}
			}
		});
		tfId.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfId.getText().compareTo(" Id" + '\0') == 0) {
					tfId.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfId.getText().compareTo(" Id" + '\0') == 0) {
					tfId.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfId.getText().compareTo(" Id" + '\0') == 0) {
					tfId.setCaretPosition(0);
				}
			}
		});
	}

	private void addMessageAddress() {
		tfAddress.setForeground(Color.LIGHT_GRAY);
		tfAddress.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfAddress.setCaretPosition(0);

			}
		});
		tfAddress.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfAddress.getText().compareTo(" Adresa" + '\0') == 0) {
					tfAddress.setText("");
				}
				tfAddress.setForeground(Color.black);
				if (tfAddress.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfAddress.setText(" Adresa" + '\0');
					tfAddress.setCaretPosition(0);
					tfAddress.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfAddress.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfAddress.getText().compareTo(" Adresa" + '\0') == 0) {
					tfAddress.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfAddress.getText().compareTo(" Adresa" + '\0') == 0) {
					tfAddress.setCaretPosition(0);
				}
			}
		});
		tfAddress.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfAddress.getText().compareTo(" Adresa" + '\0') == 0) {
					tfAddress.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfAddress.getText().compareTo(" Adresa" + '\0') == 0) {
					tfAddress.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfAddress.getText().compareTo(" Adresa" + '\0') == 0) {
					tfAddress.setCaretPosition(0);
				}
			}
		});
	}
	
	private void addMessagePassword(){
		pfPassword.setForeground(Color.LIGHT_GRAY);
		pfPassword.setEchoChar((char)0);
		pfPassword.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				pfPassword.setCaretPosition(0);

			}
		});
		pfPassword.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("deprecation")
			public void keyTyped(KeyEvent ke) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setText("");
					pfPassword.setEchoChar('*');
				}
				pfPassword.setForeground(Color.black);
				if (pfPassword.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					pfPassword.setText(" Password" + '\0');
					pfPassword.setEchoChar((char)0);
					pfPassword.setCaretPosition(0);
					pfPassword.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		pfPassword.addMouseMotionListener(new MouseMotionListener() {

			@SuppressWarnings("deprecation")
			public void mouseMoved(MouseEvent me) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}

			@SuppressWarnings("deprecation")
			public void mouseDragged(MouseEvent me) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}
		});
		pfPassword.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("deprecation")
			public void mouseClicked(MouseEvent ae) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}

			@SuppressWarnings("deprecation")
			public void mousePressed(MouseEvent ae) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}

			@SuppressWarnings("deprecation")
			public void mouseReleased(MouseEvent ae) {
				if (pfPassword.getText().compareTo(" Password" + '\0') == 0) {
					pfPassword.setCaretPosition(0);
				}
			}
		});
	}
}
