package com.butirialexandrudan.bank;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

public class AdminPanel extends JPanel {

	private static final long serialVersionUID = -6470090944414208496L;
	private MyTable myTable;
	private JTable table;
	private DefaultListModel<String> dlm;
	private JList<String> personList;
	private JButton btnAddAccount;
	private JButton btnRemoveAccount;
	private JButton btnEditAccount;
	private JButton btnCalculeazaDobanda;
	private JTextField tfCauta;
	private JScrollPane scrollPaneTable;
	private JScrollPane scrollPaneAccount;
	private GridBagConstraints gbc;

	/**
	 * Create the panel.
	 */
	public AdminPanel() {
		myTable = new MyTable(Person.class, AddThis.class,
				new ArrayList<Object>());
		table = new JTable(myTable);
		scrollPaneTable = new JScrollPane(table);
		dlm = new DefaultListModel<String>();
		personList = new JList<String>(dlm);
		scrollPaneAccount = new JScrollPane(personList);
		btnAddAccount = new JButton("Cont nou");
		btnRemoveAccount = new JButton("Sterge cont");
		btnEditAccount = new JButton("Modifica cont");
		btnCalculeazaDobanda = new JButton("Calculeaza dobanda");
		tfCauta = new JTextField(" Cauta" + '\0');
		gbc = new GridBagConstraints();

		this.setLayout(new GridBagLayout());
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.1;
		gbc.weighty = 0.1;

		gbc.gridheight = 1;
		gbc.gridwidth = 4;
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(scrollPaneTable, gbc);
		
		gbc.gridheight = 3;
		gbc.gridwidth = 3;
		gbc.gridx = 4;
		gbc.gridy = 0;
		add(scrollPaneAccount, gbc);

		gbc.weighty = 0.0;
		gbc.weightx = 0.0;
		
		gbc.gridheight = 1;
		gbc.gridwidth = 4;
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(tfCauta, gbc);
		addMessage();

		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(btnAddAccount, gbc);

		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(btnRemoveAccount, gbc);

		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 2;
		gbc.gridy = 2;
		add(btnEditAccount, gbc);
		
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 3;
		gbc.gridy = 2;
		add(btnCalculeazaDobanda, gbc);
	}

	public MyTable getMyTable() {
		return myTable;
	}

	public JTable getTable() {
		return table;
	}

	public JButton getBtnAddAccount() {
		return btnAddAccount;
	}

	public JButton getBtnRemoveAccount() {
		return btnRemoveAccount;
	}

	public JButton getBtnEditAccount() {
		return btnEditAccount;
	}

	public JButton getBtnCalculeazaDobanda() {
		return btnCalculeazaDobanda;
	}

	public JTextField getTfCauta() {
		return tfCauta;
	}
	
	public DefaultListModel<String> getDlm() {
		return dlm;
	}

	private void addMessage(){
		tfCauta.setForeground(Color.LIGHT_GRAY);
		tfCauta.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {

				tfCauta.setCaretPosition(0);

			}
		});
		tfCauta.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (tfCauta.getText().compareTo(" Cauta" + '\0') == 0) {
					tfCauta.setText("");
				}
				tfCauta.setForeground(Color.black);
				if (tfCauta.getText().length() == 0
						&& (int) ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
					tfCauta.setText(" Cauta" + '\0');
					tfCauta.setCaretPosition(0);
					tfCauta.setForeground(Color.LIGHT_GRAY);
				}

			}
		});
		tfCauta.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent me) {
				if (tfCauta.getText().compareTo(" Cauta" + '\0') == 0) {
					tfCauta.setCaretPosition(0);
				}
			}

			public void mouseDragged(MouseEvent me) {
				if (tfCauta.getText().compareTo(" Cauta" + '\0') == 0) {
					tfCauta.setCaretPosition(0);
				}
			}
		});
		tfCauta.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ae) {
				if (tfCauta.getText().compareTo(" Cauta" + '\0') == 0) {
					tfCauta.setCaretPosition(0);
				}
			}

			public void mousePressed(MouseEvent ae) {
				if (tfCauta.getText().compareTo(" Cauta" + '\0') == 0) {
					tfCauta.setCaretPosition(0);
				}
			}

			public void mouseReleased(MouseEvent ae) {
				if (tfCauta.getText().compareTo(" Cauta" + '\0') == 0) {
					tfCauta.setCaretPosition(0);
				}
			}
		});
	}
}
